package us.congcong.rsshunter.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import us.congcong.rsshunter.service.WeiboRssService;

@Api(value = "Rss服务",description="提供微博等RSS服务")
@Controller
@RequestMapping("/weiborss")
public class WeiboRssController {
	 @Autowired WeiboRssService weiboRssService;
     @ApiOperation("根据微博用户uid获取产出RSS")
     @RequestMapping(value="/uid/{uid}",method = RequestMethod.GET)  
     public ModelAndView uid(@PathVariable("uid") String uid) {
    	 Map info = weiboRssService.produceRssByUid(uid);
    	 if(info == null){
    		 return null;
    	 }
    	 ModelAndView mav = new ModelAndView();
         mav.setViewName("RSSFeedViewer");
         mav.addObject("feedChannel", info.get("channel"));
         mav.addObject("feedContent", info.get("items"));
         return mav;
     }
}